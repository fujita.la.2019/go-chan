<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Insert title here</title>
</head>
<body>
	<div id="app">
		<div>
			<input type="text" v-model="text"><input type="button" value="投稿" @click='post()'>
		</div>
		<table>
			<tr v-for="post in posts">
				<td>{{ post.text }}</td>
				<td>{{ post.timestamp }}</td>
			</tr>
		</table>
	</div>
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<script src="./js/home.js"></script>
</body>
</html>