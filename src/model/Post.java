package model;

import java.util.Date;

public class Post {
	/**
	 * 投稿のタイムスタンプ
	 */
	private Date timestamp;
	/**
	 * 投稿の本文
	 */
	private String content;

	/**
	 * コンストラクタ
	 * @param content 投稿の本文
	 */
	Post(String content) {
		this.setTimestamp(new Date());
		this.setContent(content);
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


}
